/*
 * Copyright 2018 SlavMetal <7tc at protonmail.com>
 *
 * This file is part of Just Copy URL.
 *
 * Just Copy URL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Just Copy URL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Just Copy URL. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.justcopyurl;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;

import java.util.EnumSet;

public class JustCopyURL extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the clipboard system service
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        // Get the text to share
        CharSequence originalText = getIntent().getCharSequenceExtra(Intent.EXTRA_TEXT);

        // Choose what types of links to search for
        LinkExtractor linkExtractor = LinkExtractor.builder()
                .linkTypes(EnumSet.of(LinkType.URL, LinkType.WWW))
                .build();

        // Try to extract links
        ClipData urlData = null;
        try {
            Iterable<LinkSpan> links = linkExtractor.extractLinks(originalText);

            // If there are links in the text just copy the first one
            if (links.iterator().hasNext()){
                LinkSpan link = links.iterator().next();
                urlData = ClipData.newPlainText("Message",
                        originalText.subSequence(link.getBeginIndex(), link.getEndIndex()));
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        }

        if (clipboard != null && urlData != null) {
            // Copy URL
            clipboard.setPrimaryClip(urlData);
            Toast.makeText(this, "URL copied!", Toast.LENGTH_SHORT).show();
        } else if (clipboard != null && originalText != null) {
            // Copy the whole text in case no links found
            clipboard.setPrimaryClip(ClipData.newPlainText("Message", originalText));
            Toast.makeText(this, "Original text copied!", Toast.LENGTH_SHORT).show();
        }

        finish();
    }
}

